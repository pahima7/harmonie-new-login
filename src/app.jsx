import React from 'react';
import 'normalize.css';
import SelectSharepoint from './components/SelectSharepoint'

export default class Login extends React.Component {

  style = {
    margin: "0",  
    height: "100vh",
    background: 'white'
  }

  onPremiseSelected = () => {
    this.props.history.push('/setup')
  }

  onSuccess = () => {
    this.props.history.push('/success')
  }

  render() {
    return (
      <div style={this.style}>
        <SelectSharepoint onPremiseSelected={this.onPremiseSelected} onSuccess={this.onSuccess} history={this.props.history}/>
      </div>
    );    
  }
}
