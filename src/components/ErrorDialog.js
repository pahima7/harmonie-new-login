import React from 'react';
import strings from '../translations/translations'
import { Dialog, DialogType, DialogFooter } from 'office-ui-fabric-react/lib/Dialog';
import { PrimaryButton, DefaultButton } from 'office-ui-fabric-react/lib/Button';
import style from './ErrorDialog.less';

export default class ErrorDialog extends React.Component {
    state = {
        hideDialog: false,
        errorText: ''
    };
    render() {
        const {errorTitle, errorMessage, isWarning} = this.props;
        return (
            <Dialog
            hidden={this.state.hideDialog}
            onDismiss={this._closeDialog}
            maxWidth= '390px'
            dialogContentProps={{
                type: DialogType.normal,
            }}
            modalProps={{isBlocking: true, containerClassName: 'ms-dialogMainOverride'}}>
                    <div className={isWarning ? "warning-icon" : "icon"} />
                    <div className="title">{errorTitle && errorTitle !== 'null' ? errorTitle : strings.errorTitle}</div>
                    <div className="subText">{(errorMessage && errorMessage != 'null') ? errorMessage : strings.defaultError}</div>
                <DialogFooter>
                    <DefaultButton className="supportButton" onClick={this._sendReport} text={strings.sendReportButton} />
                    <PrimaryButton className="errorButton" onClick={this._closeDialog} text="OK" />
                </DialogFooter>
            </Dialog>
        );
    }
    _showDialog = () => {
        this.setState({ hideDialog: false });
    };

    _closeDialog = () => {
        this.setState({ hideDialog: true });
    };

    _sendReport = () => {
        if(window.external.sendReport)
            window.external.sendReport();
        this._closeDialog();
    }
}