import React from 'react';
import strings from '../translations/translations'
import FadeIn from 'react-fade-in';
import style from './HoverComponent.less';
import { Callout, DirectionalHint } from 'office-ui-fabric-react';
import urlImage from '../../styles/img/url_info.png';

export default class HoverComponent extends React.Component {
  constructor(props) {
    super(props);
    this.handleMouseHover = this.handleMouseHover.bind(this);
    this.state = {
      isHovering: false,
    };
  }

  handleMouseHover() {
    this.setState(this.toggleHoverState);
  }

  toggleHoverState(state) {
    return {
      isHovering: !state.isHovering,
    };
  }
  
  render() {
    const iconClass = this.props.url ? "-url" : ""
    return (
      <div className="hover-wrapper" onMouseEnter={this.handleMouseHover} onMouseLeave={this.handleMouseHover}>
        <div className={"icon-container" + iconClass} ref={icon => (this._icon = icon)}>
            <div className="infoIcon" />
        </div>
        { (this.state.isHovering && this.props.url) && 
           <Callout
           className="urlImageCallout"
            role={'alertdialog'}
            gapSpace={7}
            target={this._icon}
            onDismiss={this._onCalloutDismiss}
            setInitialFocus={true}
            directionalHint={DirectionalHint.bottomRightEdge}>
            <img className="urlImage" src={urlImage} alt="URL Info"/>
           </Callout>
        }
        { (this.state.isHovering && this.props.user) &&
          <div>
            <Callout
            className="ms-CalloutExample-callout"
            role={'alertdialog'}
            gapSpace={3}
            target={this._icon}
            onDismiss={this._onCalloutDismiss}
            setInitialFocus={true}
            directionalHint={DirectionalHint.rightCenter}>
              <div>
                <p className="hovered-text">
                  {strings.usernameHint}
                </p>
              </div>
            </Callout>
        </div> }
      </div>
    );
  }
}