import React from 'react';
import style from './Loading.less';

function preventDefault(evt){
  evt.preventDefault()
}

export default ({absolute, floating, text, error, invertTextColors}) => (
  <div className={"cssload-placment" + (absolute ? ' absolute' : '')} onTouchStart={preventDefault} onMouseDown={preventDefault}>
    {floating && <div className="cssload-mask"></div>}
    <div className="cssload-loader">
      <div className="cssload-inner cssload-one"></div>
      <div className="cssload-inner cssload-two"></div>
      <div className="cssload-inner cssload-three"></div>
    </div>
    {error && <div className="text ms-font-s ms-fontColor-error">{error}</div>}
    {!error && text && <div className={`text ms-font-s ${invertTextColors ? 'invert-colors' : ''}`}>{text}</div>}
  </div>
);
