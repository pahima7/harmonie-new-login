import React from 'react';
import { Fabric } from 'office-ui-fabric-react/lib/Fabric';
import { DefaultButton} from 'office-ui-fabric-react/lib/Button';
import { Label } from 'office-ui-fabric-react/lib/Label';
import { Callout, DirectionalHint } from 'office-ui-fabric-react';
import Loading from './Loading';
import strings from '../translations/translations';
import style from './SelectSharepoint.less';
import notSureImage from '../../styles/img/how_to_info.png'
import ErrorDialog from './ErrorDialog'

class SelectSharepint extends React.Component {

    constructor(props) {
        super(props);
        //Init globals for Java usage
        harmonieApi.loading = () => this.setState({status: 'loading', error: ''})
        harmonieApi.idle = () => this.setState({status: 'idle', error: ''})
        harmonieApi.success = () => { props.history.push('/success') };
        // harmonieApi.error = (error) => this.setState({status: 'idle', error: error})
        harmonieApi.error = (error) => this.setState({status: 'error', error: error})
        this.handleMouseHover = this.handleMouseHover.bind(this);
        this.onAddCloud.bind(this)
    }

    state = {
        status: 'idle',
        notSure: false
    }

    handleMouseHover() {
        this.setState(this.toggleHoverState);
    }

    toggleHoverState(state) {
        return {
            notSure: !state.notSure,
        };
    }

    onAddCloud() {
        const addCloud = new Promise((resolve, reject) => {
            this.setState({status : 'loading'})
            try {
                window.external.addCloud();
            } catch(err) {
                reject()
            }
        })
        addCloud({}, () => this.setState({status: 'idle', error: ''}))
    }

    render() {
        const {onPremiseSelected} = this.props;
        const {status} = this.state;
        return (
            <div className="container">
                { status === 'loading' &&  <Loading /> }
                <div style={{"display": "flex", "justifyContent": "center", "paddingTop": "20px"}}>
                    <Label className="title" style={{margin: "15px 0 5px 0px", textAlign: "center", color:"#0078D4", fontSize:"22px", fontWeight: 600}}>{strings.welcome}</Label>
                </div>
                <Fabric className="buttonsContainer">
                    <DefaultButton className="office" style={{borderColor:"#DC3E15"}} onClick={this.onAddCloud.bind(this)}>
                        <div className="buttonImg office"></div>
                        <Label className="buttonLabel" style={{color: "#DC3E15"}}>{strings.connectSharepoint}<b>{strings.Online}</b></Label>
                    </DefaultButton>
                    <DefaultButton className="sharepoint" style={{borderColor:"#1672B8"}} onClick={onPremiseSelected}>
                        <div className="buttonImg sharepoint"></div>
                        <Label className="buttonLabel" style={{color: "#1672B8"}}>{strings.connectSharepoint}<b>{strings.OnPremise}</b></Label>
                    </DefaultButton>
                </Fabric>
                <div class="notSureLine">
                    <div className="hoverNotSure" onMouseEnter={this.handleMouseHover} onMouseLeave={this.handleMouseHover}>
                        <div className="notSure" ref={icon => (this._icon = icon)}>
                            <div className="icon-container-not-sure">
                                <div className="infoIcon" />
                            </div>
                            <div className="link">{strings.notSureHowConnect}</div>
                        </div>
                        { (this.state.notSure) &&
                            <Callout
                            className="notSureCallout"
                            role={'alertdialog'}
                            target={this._icon}
                            onDismiss={this._onCalloutDismiss}
                            setInitialFocus={true}
                            directionalHint={DirectionalHint.topCenter}>
                            <img className="notSureImage" src={notSureImage} alt="Connection info"/>
                            </Callout>
                        }
                    </div>
                </div>
                {status === 'error' && <ErrorDialog isWarning={true} errorMessage={strings.warningText} errorTitle={strings.warningTitle}/>}
            </div>
            );
    }
}

export default SelectSharepint;
