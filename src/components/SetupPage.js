import React from 'react';
import strings from '../translations/translations';
import { Fabric } from 'office-ui-fabric-react/lib/Fabric';
import { Label } from 'office-ui-fabric-react/lib/Label';
import { TextField, ITextFieldStyleProps, ITextFieldStyles } from 'office-ui-fabric-react/lib/TextField';
import { ChoiceGroup } from 'office-ui-fabric-react/lib/ChoiceGroup';
import { DefaultButton } from 'office-ui-fabric-react/lib/Button';
import { Icon } from 'office-ui-fabric-react/lib/Icon';     
import Loading from './Loading';
import FadeIn from 'react-fade-in';
import HoverComponent from './HoverComponent'
import ErrorDialog from './ErrorDialog'
import style from './SetupPage.less';

export default class SetupPage extends React.Component {
    
    constructor(props) {
        super(props);
        //Init globals for Java usage
        harmonieApi.loading = () => this.setState({status: 'loading', error: ''})
        harmonieApi.idle = () => this.setState({status: 'idle', error: ''})
        harmonieApi.success = () => props.history.push('/success');
        harmonieApi.error = (error) => this.setState({status: 'error', error: error})
        this._getUserNamePasswordErrorMessage = this._getUserNamePasswordErrorMessage.bind(this);
        this._getURLErrorMessage = this._getURLErrorMessage.bind(this);
    }

    state = {
        username: '',
        password: '',
        siteUrl: '',
        status: 'idle',
        selectedKey: '',
        OKButtonDisabled: true,
        hasError: true,
    }

    inputRef = null

    setInputRef = ref => {
        this.inputRef = ref
      }
      
    componentDidMount = () => { 
        if (this.inputRef) {
            this.inputRef.focus()
        }
    }
    keyPress = (e) => {
        const {OKButtonDisabled} = this.state;
        if(e.keyCode == 13 && !OKButtonDisabled) {
            this.onOk();
        }
    }

    validateForm() {
        const {selectedKey, username, password, siteUrl} = this.state;
        if (siteUrl && this.validateURL(siteUrl) == '') {
            if ((selectedKey == 'browser' || selectedKey == 'integrated')) {
                this.setState({OKButtonDisabled : false});
                return;
            } else if (selectedKey === 'custom') {
                if (this.validateUserNamePassword(username) == '' && this.validateUserNamePassword(password) == '') {
                    this.setState({OKButtonDisabled : false});
                    return;
                }
            }
        }
        this.setState({OKButtonDisabled : true});
        return;
    }

    validateUserNamePassword(value) {
        if (!value) {
            return 'empty'
        } else {
            return ''
        }
    }

    validateURL(url) {
        var urlRegex = /^(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?$/i;
        if (urlRegex.test(url)) {
            return ''
        }
        return strings.errorUrlNotValid
    }

    onChange(event, option) {
        this.setState({selectedKey : option.key}, () => {this.validateForm()});
    }

    handleUserNameChange(event) {
       this.setState({username : event.target.value}, () => {this.validateForm()})
    }

    handlePWChange(event) {
        this.setState({password : event.target.value}, () => {this.validateForm()})
    }

    handleSiteUrlChange(event) {
        this.setState({siteUrl : event.target.value}, () => {this.validateForm()})

    }

    _getURLErrorMessage(value) {
        var err = '';
        if (!value) {
            err = strings.errorUrlEmpty
            this.setState({errorUrl : strings.errorUrlEmpty});
        }
        else {
            err = this.validateURL(value)
            this.setState({errorUrl : err});
        }
        return err;
    }

    _getUserNamePasswordErrorMessage(value) {
        const err = this.validateUserNamePassword(value)
        if (err == 'empty') {
            return strings.errorNamePasswordEmpty
        } else {
            return ''
        }
    }

    onOk() {
        const {selectedKey, username, siteUrl, password} = this.state;                
        const addSite = new Promise((resolve, reject) => {
            this.setState({status : 'loading'})
            try {                
                window.external.addSite(siteUrl, selectedKey, username, password)                            
            } catch(err) {
                reject()
            }
        })
       addSite()
    }

    back() {
        this.props.history.push('/')
    }

    render() {  
        const {selectedKey, status, OKButtonDisabled, error, username, password} = this.state;
        return (
        <div className="window-wrapper">
            <div className="page-wrapper" onKeyDown={this.keyPress}>
                <div className="back" onClick={() => this.back()}>
                    <Icon iconName="Back" className="ms-Icon ms-Icon--Back"></Icon>
                </div>
                {status === 'loading' && <Loading text="Add site..."/>}
                <div style={{ "display": "flex", "justifyContent": "center", "paddingTop": "20px"}}>
                    <Label className="setupTitle" style={{margin: "15px 0 5px 0px", color: "#2b88d8", textAlign: "center", fontSize:"22px", fontWeight: 600}}>{strings.setupTitle}</Label>
                </div>
                <Fabric className="content-cntainer">
                    <div className="url-container">
                        <Label className="url-label">{strings.setupInstructions}</Label>
                        <br/>
                        <div className="url-field">
                            <div className="url-text">
                                <TextField underlined placeholder={strings.urlPlaceholder} onChange={this.handleSiteUrlChange.bind(this)} onGetErrorMessage={this._getURLErrorMessage} validateOnLoad={false} validateOnFocusOut/>
                            </div>
                            <HoverComponent url={true}/> 
                        </div>
                    </div>
                    <ChoiceGroup className="choice-group"
                        style={{position : "absolute"}}
                        options={[
                                {
                                    key: 'integrated',
                                    text: strings.windowsCredentials,
                                    'data-automation-id': 'auto1',
                                    onRenderLabel : (option) => {
                                        if (option.checked) {
                                            return <span style={{paddingLeft:"28px", fontWeight: "bold"}}>{option.text}</span>
                                        }
                                        return <span style={{paddingLeft:"28px"}}>{option.text}</span>
                                    }
                                },
                                {
                                    key: 'browser',
                                    text: strings.browserAuth,
                                    onRenderLabel : (option) => {
                                        if (option.checked) {
                                            return <span style={{paddingLeft:"28px", fontWeight: "bold"}}>{option.text}</span>
                                        }
                                        return <span style={{paddingLeft:"28px"}}>{option.text}</span>
                                    }
                                },
                                {
                                    key: 'custom',
                                    text: strings.customCredentials,
                                    onRenderLabel : (option) => {
                                        if (option.checked) {
                                            return <span style={{paddingLeft:"28px", fontWeight: "bold"}}>{option.text}</span>
                                        }
                                        return <span style={{paddingLeft:"28px"}}>{option.text}</span>
                                    }
                                }
                        ]}
                        onChange={this.onChange.bind(this)}
                        ariaLabelledBy="labelElement"
                        required={true}
            />
            {
                selectedKey === 'custom' &&
                <div className="user-password">
                    <FadeIn>
                        <div className="userName">
                            <Label className="userNameLabel">{strings.username}</Label>
                            <TextField value={this.username} onChange={this.handleUserNameChange.bind(this)} value={username} onGetErrorMessage={this._getUserNamePasswordErrorMessage} validateOnLoad={false} validateOnFocusOut underlined />
                            <HoverComponent user={true}/> 
                        </div>
                        <div className="password">
                            <Label className="passwordLabel">{strings.password}</Label>
                            <TextField type="password" value={this.password} onChange={this.handlePWChange.bind(this)} value={password} onGetErrorMessage={this._getUserNamePasswordErrorMessage} validateOnLoad={false} validateOnFocusOut underlined />
                        </div>
                    </FadeIn>                
                </div>
            }
            </Fabric>
            <DefaultButton className={"OKBtn" + (OKButtonDisabled ? "" : "-enabled")} primary={false} disabled={OKButtonDisabled} onClick={this.onOk.bind(this)}>{strings.done}</DefaultButton>
        </div>
        {status === 'error' && <ErrorDialog errorMessage={error}/>}
    </div>
        );   
    }
}