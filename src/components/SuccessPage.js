import React from 'react';
import { Label } from 'office-ui-fabric-react/lib/Label';
import { PrimaryButton, ActionButton } from 'office-ui-fabric-react/lib/Button';
import Loading from './Loading';
import strings from '../translations/translations';
import style from './SuccessPage.less';

export default class SuccessPage extends React.Component {
    state = {
        hideDialog: false,
        isLoading : false,
    };
    closeWindow = () => {
       window.external.closeDialog();
    };
    openVideoLink = () => {
        window.location = 'https://harmon.ie/start-working-harmonie-video-hook'
        if (document.readyState == 'loading') {
            this.setState({isLoading : true});
            return
        }
    }
    render() {
        const {isLoading} = this.state;
        return (
            <div className="window-wrapper">
                { isLoading == true &&  <Loading /> }
                { isLoading == false && 
                    <div className="container">
                        <div>
                        <div style={{ "display": "flex", "justifyContent": "center", "paddingTop": "20px"}}>
                            <Label className="successTitle" style={{margin: "15px 0 5px 0px", textAlign: "center", color:"#0078D4", fontSize:"22px", fontWeight: 600}}>{strings.harmonieReady}</Label>
                        </div>
                            <Label style={{"marginTop": "13px", "marginLeft": "10px", "textAlign": "center", "fontSize":"16px",}}>{strings.successText}</Label>
                        </div>
                        <div className="image"/>
                        <div className="footerContainer">
                            <PrimaryButton className="successButton" text={strings.gotIt} onClick={this.closeWindow}/>
                            <ActionButton className="successLink" onClick={this.openVideoLink}>
                            <div className="successLinkContainer">
                                <div className="successLinkImage"/>
                                <div className="successLinkText">{strings.showHowWorks}</div>
                            </div>
                            </ActionButton>
                        </div>
                    </div>
                }
            </div>
        );
    }
}