import React from 'react';
import { Icon } from 'office-ui-fabric-react/lib/Icon'
import { Label } from 'office-ui-fabric-react/lib/Label';
import style from './URLLink.less';

export default class URLLink extends React.Component {
    
    constructor(props) {
        super(props);
    }

    back() {
        this.props.history.push('/')
    }

    render() {
        return (
            <div className="window-wrapper">
                <div style={{"display": "flex", "flexDirection": "column", "textAlign": "center", "height": "550px" }}>
                <div className="back" onClick={() => this.back()}>
                    <Icon iconName="Back" className="ms-Icon ms-Icon--Back"></Icon>
                </div>
                <iframe width="560" height="315" src="https://harmon.ie/start-working-harmonie-video-hook" frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"></iframe>
                </div>
            </div>
        );
    }
}
