import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom';
import Login from './app';
import SetupPage from './components/SetupPage'
import SuccessPage from './components/SuccessPage'
import URLLink from './components/URLLink'

const Routes = () => (
  <Router>
    <Switch>
      <Route path="/setup" component={SetupPage}/>
      <Route path="/success" component={SuccessPage}/>
      <Route path="/urlLink" component={URLLink}/>
      <Route component={Login}/>
    </Switch>
  </Router>
);

export default Routes;
