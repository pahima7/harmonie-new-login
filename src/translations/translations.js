
import LocalizedStrings from 'react-localization';

export default new LocalizedStrings({
    en: {
        //SelectSharepoint
        welcome: "Let's Get Started",
        connectSharepoint: "Connect to SharePoint ",
        Online: "Online",
        OnPremise: "on-premises",
        notSureHowConnect: "Not sure what to select?",
        //SetupPage
        setupTitle: "Connect to your SharePoint site",
        setupInstructions: "Please type the SharePoint URL or copy from your browser and paste it here:",
        urlPlaceholder: "SharePoint address goes here (Ctrl+V to paste)",
        windowsCredentials: "Login with your Windows credentials",
        browserAuth: "Use the browser to connect",
        customCredentials: "Provide username and password",
        username: "Username:",
        password: "Password:",
        errorUrlEmpty: "URL can not be empty.",
        errorNamePasswordEmpty: "This field can not be empty.",
        errorUrlNotValid: "This URL is not valid.",
        done: "Done",
        //HoverComponent
        usernameHint: "Login with either your assigned username or your email address.",
        //ErrorDialog
        errorTitle: "Failed to connect to SharePoint",
        warningTitle: "The process is not complete",
        warningText: "Click “OK” to return and try again. If you can’t login click “Send report”.",
        defaultError: "There was a problem connecting to SharePoint. Please try again or contact your administrator if the problem persists.",
        sendReportButton: "Send Report",
        //successPage
        harmonieReady: "harmon.ie is ready!",
        successText: "Start by drag and drop emails to the harmon.ie sidebar to save them in SharePoint.",
        gotIt: "OK, got it",
        showHowWorks: "Show me how it works",
    },
    fr: {
        //SelectSharepoint page
        welcome: "C'est parti",
        connectSharepoint: "Se connecter à SharePoint ",
        Online: "En ligne",
        OnPremise: "Sur site",
        notSureHowConnect: "Vous ne savez pas quoi choisir ?",
        //SetupPage
        setupTitle: "Se connecter à votre site SharePoint",
        setupInstructions: "Veuillez saisir l'URL SharePoint ou la copier depuis votre navigateur et la coller ici :",
        urlPlaceholder: "Saisissez l'adresse SharePoint ici (Ctrl+V pour la coller)",
        windowsCredentials: "Connectez-vous avec vos identifiants Windows",
        browserAuth: "Utilisez le navigateur pour vous connecter",
        customCredentials: "Saisir le nom d'utilisateur et le mot de passe",
        username: "Nom d'utilisateur :",
        password: "Mot de passe :",
        errorUrlEmpty: "L'URL ne peut pas être vide.",
        errorNamePasswordEmpty: "Ce champ ne peut pas être vide.",
        errorUrlNotValid: "Cette URL n'est pas valide.",
        done: "Terminé",
        //HoverComponent
        usernameHint: "Connectez-vous avec le nom d'utilisateur qui vous a été attribué ou avec votre adresse e-mail.",
        //ErrorDialog
        errorTitle: "Echec de connexion à SharePoint",
        warningTitle: "Le processus n'est pas terminé",
        warningText: "Cliquez sur \"OK\" pour revenir en arrière et réessayer. Si vous ne parvenez pas à vous connecter, cliquez sur \"Envoyer un rapport\".",
        defaultError: "Un problème est survenu pendant la connexion à SharePoint. Veuillez réessayer ou contactez votre administrateur si le problème persiste.",
        sendReportButton: "Envoyer le rapport",
        //successPage
        harmonieReady: "harmon.ie est prêt !",
        successText: "Commencez par faire glisser les e-mails dans la barre latérale d'hamon.ie pour les enregistrer dans SharePoint.",
        gotIt: "D'accord, j'ai compris",
        showHowWorks: "Montrez-moi comment ça fonctionne",
    },
    de: {
        //SelectSharepoint
        welcome: "Es kann beginnen",
        connectSharepoint: "Mit SharePoint verbinden ",
        Online: "Online",
        OnPremise: "Lokal",
        notSureHowConnect: "Sind Sie nicht sicher, was Sie auswählen möchten?",
        //SetupPage
        setupTitle: "Verbinden Sie sich mit Ihrer SharePoint-Website",
        setupInstructions: "BItte geben Sie die SharePoint-URL ein oder kopieren Sie diese aus Ihrem Browser und fügen Sie sie hier ein:",
        urlPlaceholder: "SharePoint-Adresse hier eingeben (Strg+V zum Einfügen)",
        windowsCredentials: "Anmeldung mit Ihren Windows-Zugangsdaten",
        browserAuth: "Verbinden Sie sich mit dem Browser",
        customCredentials: "Geben Sie einen Benutzernamen und Kennwort an",
        username: "Benutzername:",
        password: "Kennwort:",
        errorUrlEmpty: "Die URL darf nicht leer sein.",
        errorNamePasswordEmpty: "Dieses Feld darf nicht leer sein.",
        errorUrlNotValid: "Diese URL ist nicht gültig.",
        done: "Fertig",
        //HoverComponent
        usernameHint: "Melden Sie sich mit Ihrem zugewiesenen Benutzernamen oder Ihrer E-Mail-Adresse an.",
        //ErrorDialog
        errorTitle: "Verbindung mit SharePoint fehlgeschlagen",
        warningTitle: "Der Prozess ist nicht abgeschlossen",
        warningText: "Klicken Sie auf “OK”,  um zurückzukehren und es erneut zu versuchen. Falls Sie sich nicht anmelden können, klicken Sie bitte auf “Bericht senden”",
        defaultError: "Leider besteht ein Problem beim Herstellen der Verbindung mit SharePoint. Wenden Sie sich an den Administrator, falls das Problem weiterhin besteht.",
        sendReportButton: "Bericht senden",
        //successPage
        harmonieReady: "harmon.ie ist bereit!",
        successText: "Beginnen Sie mit Ziehen und Ablegen von E-Mails auf die harmon.ie-Randleiste, um diese in SharePoint zu speichern.",
        gotIt: "OK, ich habe es verstanden",
        showHowWorks: "Zeig mir, wie es funktioniert",
    }
});